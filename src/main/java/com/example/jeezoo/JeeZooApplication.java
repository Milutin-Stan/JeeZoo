package com.example.jeezoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeeZooApplication {

  public static void main(String[] args) {
    SpringApplication.run(JeeZooApplication.class, args);
  }

}
