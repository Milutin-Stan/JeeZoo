package com.example.jeezoo.zoo.infrastructure.primary.request;

public class AddZooRequest {

    public String name;
    public String location;
    public Float size;
    public String spaceCapacity;
    public String peopleCapacity;
    public String zooStatus;
}
