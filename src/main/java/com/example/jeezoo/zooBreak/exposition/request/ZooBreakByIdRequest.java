package com.example.jeezoo.zooBreak.exposition.request;

import javax.validation.constraints.NotNull;

public final class ZooBreakByIdRequest {
    @NotNull
    public Long id;
}
